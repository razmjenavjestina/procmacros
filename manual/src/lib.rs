extern crate proc_macro;
use proc_macro::*;
use std::iter::FromIterator;

#[proc_macro]
pub fn return_two(items: TokenStream) -> TokenStream {
    let mut code = String::new();
    for item in items {
        match item {
            TokenTree::Ident(i) => {
                code.push_str(&format!("fn {} () -> u32 {{ 2 }}", i))
            }
            _ => panic!("item expected")
        }

    }
    println!("{}", code);
    let ts = code.parse().unwrap();
    println!("{:?}", ts);
    ts
}

fn make_id(name: &str) -> TokenStream {
    let mut arg = vec![
        TokenTree::Ident(Ident::new("x", Span::call_site())),
        TokenTree::Punct(Punct::new(':', Spacing::Alone)),
        TokenTree::Ident(Ident::new("u32", Span::call_site())),
    ];
    let arg = TokenTree::Group(Group::new(Delimiter::Parenthesis, TokenStream::from_iter(arg.drain(..))));

    let mut body = vec![
        TokenTree::Ident(Ident::new("x", Span::call_site())),
    ];

    let body = TokenTree::Group(Group::new(Delimiter::Brace, TokenStream::from_iter(body.drain(..))));

    let mut f  = vec![
        TokenTree::Ident(Ident::new("fn", Span::call_site())),
        TokenTree::Ident(Ident::new(name, Span::call_site())),
        arg,
        TokenTree::Punct(Punct::new('-', Spacing::Joint)),
        TokenTree::Punct(Punct::new('>', Spacing::Alone)),
        TokenTree::Ident(Ident::new("u32", Span::call_site())),
        body,
    ];

    let ts = TokenStream::from_iter(f.drain(..));
    println!("{:?}", ts);
    ts
}


#[proc_macro]
pub fn ids(items: TokenStream) -> TokenStream {
    let mut ts = TokenStream::new();
    for item in items {
        match item {
            TokenTree::Ident(i) => {
                ts.extend(make_id(&i.to_string()));
            }
            _ => panic!("item expected")
        }

    }
    ts
}
