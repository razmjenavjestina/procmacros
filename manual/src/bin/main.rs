use manual::*;

return_two!(sve ove funkcije vracaju dva);
ids!(a c);

fn main() {
    assert_eq!(sve(), 2);
    assert_eq!(ove(), 2);
    assert_eq!(funkcije(), 2);
    assert_eq!(vracaju(), 2);
    assert_eq!(dva(), 2);

    assert_eq!(1, a(1));
    assert_eq!(2, c(c(2)));
}
