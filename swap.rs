macro_rules! swap {
    ($a:ident, $b:ident) => {
        let t = $a;
        $a = $b;
        $b = t;
    }
}

fn main() {
    let mut t = 1;
    let mut p = 2;
    println!("{} {}", t, p);
    swap!(t, p);
    println!("{} {}", t, p);
}
