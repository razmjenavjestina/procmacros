extern crate proc_macro;

use std::fs;
use std::io::Write;
use std::process;
use proc_macro::TokenStream;

static mut CNT: usize = 0;

macro_rules! pr {
    ($v:expr) => { println!("expr: {}", $v) }
}

macro_rules! macro_used {
    () => { unsafe { CNT += 1; pr!(CNT) } }
}

fn dump_pid(path: &str) {
    let mut fp = fs::File::create(path).unwrap();
    pr!(process::id());
    fp.write_fmt(format_args!("PID: {}\n", process::id())).unwrap();
}

#[proc_macro]
pub fn m1(_items: TokenStream) -> TokenStream {
    macro_used!();
    dump_pid("/tmp/macro.pid");
    "fn foo() -> u32 { 42 }".parse().unwrap()
}

#[proc_macro]
pub fn m2(_items: TokenStream) -> TokenStream {
    macro_used!();
    "fn bar() {}".parse().unwrap()
}
