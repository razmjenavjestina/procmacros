use pid::{m1,m2};

macro_rules! swap {
    ($a:ident, $b:ident) => {
        let a = $a;
        $a = $b;
        $b = a;
    };
    ($a:ident) => {};
}

m1!();
m2!();

fn main() {
    let mut a = 1;
    let mut b = 2;
    println!("a: {}, b: {}", a, b);
    swap!(a, b);
    println!("a: {}, b: {}", a, b);
}
