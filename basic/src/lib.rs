extern crate proc_macro;
use proc_macro::TokenStream;

#[proc_macro]
pub fn m1(_items: TokenStream) -> TokenStream {
    "fn foo() -> u32 { 42 }".parse().unwrap()
}
