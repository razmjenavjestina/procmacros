extern crate proc_macro;

use proc_macro::*;

//proc_macro2
fn ret42(itemfn: &syn::ItemFn) -> TokenStream {
    let funame = &itemfn.sig.ident;
    let body = &itemfn.block;
    let code = quote::quote! {
        fn #funame () -> u32 { 43 }
        fn bar () -> u32 #body
    };
    TokenStream::from(code)
}

#[proc_macro]
pub fn stole_name(items: TokenStream) -> TokenStream {
    match syn::parse_macro_input!(items as syn::Item) {
        syn::Item::Fn(itemfn) => ret42(&itemfn),
        _ => panic!("must be function")
    }
}
