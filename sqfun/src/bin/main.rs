use sqfun::stole_name;

stole_name!(fn foo() -> u32 { 42 });

fn main() {
    assert_eq!(foo(), 43);
    assert_eq!(bar(), 42);
}
