use basic::m1;

macro_rules! m1exp {
    () => {
        {
            struct A;
            impl A { m1!(); }
            A::foo()
        }
    }
}

fn main() {
    let a = {
        struct A;
        impl A { fn foo() -> u32 { 41 } }
        A::foo()
    };
    assert_eq!(a, 41);

    let b = m1exp!();
    assert_eq!(b, 42);
}
