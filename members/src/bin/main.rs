#![allow(dead_code)]
use members::Members;

#[derive(members::Members)]
struct Fofo {
    x: u32,
    y: f64,
    z: String,
}

struct Quaz;

impl members::Members for Quaz {
    fn members() -> Vec<&'static str> { vec! [ "krivi", "memberi" ] }
}

fn main() {
    println!("{:?}", Fofo::members());
    println!("{:?}", Quaz::members());
}
