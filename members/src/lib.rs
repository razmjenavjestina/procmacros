pub use members_derive::*;

pub trait Members {
    fn members() -> Vec<&'static str>;
}
