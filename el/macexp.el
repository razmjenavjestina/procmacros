(defmacro multidef (name rule)
  `(defmacro ,name (&rest syms)
     `(progn ,@(mapcar ,rule syms))))

(multidef return-two-2 (lambda (sym) `(defun ,sym () 2)))
(multidef ids-2 (lambda (sym) `(defun ,sym (x) x)))

(return-two-2 a2 b2 c2)
(ids-2 x2 y2 z2)
