(defmacro return-two (&rest syms)
  `(progn
     ,@(mapcar (lambda (sym) `(defun ,sym nil 2)) syms)))

(return-two a b c)

(defmacro ids (&rest syms)
  `(progn
     ,@(mapcar (lambda (sym) `(defun ,sym (x) x)) syms)))

(ids x y z)
