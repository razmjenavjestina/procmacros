#+STARTUP: content hidestars indent

* Proceduralni makroi

** Što je makro?

- Nova sintaksna forma

- Apstrakcije DSL-om

- Održavanje minimalne sintakse jezika


** Deklarativni vs Proceduralni (a.k.a. Higijenski vs Nehigijenski)

- [[./swap.rs][Primjer higijenskog makroa]]

- =gensym=


** Proceduralni makroi 101

*** Novi tip cratea


*** =proc_macro= crate


- =TokenTree=

- =TokenStream=

- =Span=


*** [[./basic][Primjer za zagrijavanje]]


*** Tri vrste proceduralnih makroa

- Function alike

- Attribute

- Derive


*** Item position


** Makro ekspanzija

- Usporedba sa =build.rs=

- [[./pid][Kontekst izvršavanja kroz primjer]]


** Tehnike pisanja makroa

*** [[./manual][Ručno pisani netrivijalni makro]]


*** =syn= i =quote= crateovi

- [[https://crates.io/crates/syn][syn crate]]

- [[https://crates.io/crates/quote][quote crate]]

- [[./sqfun][Primjer function alike makroa]]

- [[./members_derive][Primjer derive makroa]]


*** Expression expansion hack

- [[./expexp][Primjer expression expansiona]]

- [[https://crates.io/crates/proc-macro-hack]['Službeni' hack]]


** Gdje je tu Lisp?

- [[./el/basic.el][Ergonomičnost]]

- [[./el/macexp.el][Makro expanding makro]]

- Faze ekspanzije

- Proceduralno => Deklarativnom
