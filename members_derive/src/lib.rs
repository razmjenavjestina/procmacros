extern crate proc_macro;

use proc_macro::*;

fn implement_members_trait(name: &syn::Ident,
                           fields: &syn::FieldsNamed) -> TokenStream {
    let members: Vec<_> = fields.named.iter().map(|f| {
        f.ident.as_ref().unwrap().to_string()}
    ).collect();
    let code = quote::quote! {
        impl ::members::Members for #name {
            fn members() -> Vec<&'static str> { vec![#(#members),*] }
        }
    };
    TokenStream::from(code)
}

#[proc_macro_derive(Members)]
pub fn members_derive(items: TokenStream) -> TokenStream {
    let di = syn::parse_macro_input!(items as syn::DeriveInput);
    match di.data {
        syn::Data::Struct(ds) => {
            match ds.fields {
                syn::Fields::Named(nf) => implement_members_trait(&di.ident, &nf),
                _ => panic!("must be named")
            }
        }
        _ => panic!("must be struct")
    }
}
